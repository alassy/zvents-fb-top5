FACEBOOK_APP_ID = "64db5610063592b19cd7913a85aac78a"
FACEBOOK_APP_SECRET = "3db24093a7572749bcba7caa314b3b12"

LIKE_POST_LINK = "http://zvlists.appspot.com/"
FB_APP_LINK = "http://apps.facebook.com/top__five/"

IS_LIVE = False
DEBUG = False
#IS_LIVE = True

APP_LINK = "http://localhost:8080/"
if IS_LIVE:
    APP_LINK = "http://zvlists.appspot.com/"


LIST_ALL = 0
LIST_FRIENDS = 6
LIST_ME = 7
LIST_CAT = 8
LIST_SEARCH = 9

CAT_VENUES = 5
CAT_RESTAURANTS = 1
CAT_BARSCLUBS = 2
CAT_MOVIES = 3
CAT_PERFORMERS = 4

ZV_CATEGORIES = {"cats": [
    {
        "cat":"0",
        "hint":"e.g. \"Live Music\" or \"Cirque du Soleil\"",
        "lnk":"thingstodo",
        "name":"Things to Do",
        "topics": [
"Live Music",
"Festivals through the Year",
"Fun Places to take the Kids",
"Best Kept Secrets",
"Places For the Broke and the Bored",
"Favorite Places",
"FREE things to Do",
"Cultural Activities",
"Weekender",
"Tourist Hot Spots",
"Active Life & Outdoor Activities",
"Activities with Toddlers",
"Date Spots"
        ]

    },{
        "cat":1,
        "hint":"e.g. \"Sushi\" or \"Steakhouse\"",
        "name":"Restaurants",
        "lnk":"restaurants",
        "setName": "venues",
        "topics": [
"Lunch Spots",
"Cafe Crawlers",
"Yogurt Lovers",
"Kids-Friendly Restaurants",
"Cheap Eats",
"Dessert Spots",
"Weekend Brunches",
"Hole in the Wall Restaurants",
"Casual Date Restaurants",
"Things to Eat & Drink before you Die",
"Romantic Restaurants",
"Dim Sum Places",
"Sushi",
"Steakhouses",
"BBQ House"
        ]
    },{
        "cat":2,
        "hint":"e.g. \"Wine Bar\" or \"Irish Pub\"",
        "name":"Bars and Clubs",
        "lnk":"barsandclubs",
        "topics": [
"Hot Bars",
"Bars & Clubs for Girls Night Out",
"Salsa Dancing",
"Dive Bars",
"Best Bartenders",
"Places I like to go to Dance",
"Clubs and Lounges",
"Fun Places for Happy Hour",
"Cocktail Bars",
"Sexiest Hotel Lounges",
"Boys Night Out",
"Gay Bars & Clubs",
"Bar Hopping",
"Single Bars",
"Karaoke Clubs"
        ]
    },{
        "cat":3,
        "hint":"e.g. \"Avatar\" or \"Toy Story 3\"",
        "name":"Movies",
        "lnk":"movies",
        "topics": [
"Best Movies Ever!",
"Tearjerker Movies of All time",
"Movies that I Watched Over and Over again",
"Childhood Movies That I Love Even More Now",
"Comedy Flicks",
"Movies That I'm Embarrassed About Enjoying", 
"Cool and Unusual Movies for College Guys",
"Super Hero Movies",
"Movies of 2010",
"Romantic Dramas",
"Popcorn Movies",
"Scariest Movies I Watched",
"Comedy Movies",
"Animated Movies"
        ]
    },{
        "cat":4,
        "hint":"e.g. \"Lady Gaga\" or \"U2\"",
        "name":"Performers",
        "lnk":"performers",
        "topics": [
"Concerts I've been to",
"Comedians",
"Pop Concerts",
"Theatrical Shows",
"Kickin' It Old School Performers",
"Sport Teams",
"Hip Hop",
"Off-Broadway Shows",
"Rock Bands I've Seen",
"Ballet & Dance Performances",
"Kids and Family Shows",
"Open Mic Nights",
"Disney Shows",
"Outdoor Shows",
"Musicals"
        ]
    },{
        "cat":5,
        "hint":"e.g. \"Golden Gate Bridge\" or \"Statue of Liberty\"",
        "name":"Places",
        "lnk":"places",
        "topics": [
"Manicures & Pedicures",
"Spa-licious Places",
"Romantic Getaways",
"Epicurean Tours",
"Travel Bucket List",
"Pet Friendly Parks",
"Comedy Clubs",
"I Heart Music Venues",
"Fun Places 18+",
"Places to avoid like the Plague",
"Dating Spots",
"Music Performances",
"Shopping Malls",
"Places to take out of town guests",
"Breakfast Spots"
        ]
    }
]}

ZV_SUBNAV = [
        {'id':5,'lnk':'places', 'nm': 'Places', 'on':'0' },
        {'id':4,'lnk':'performers', 'nm': 'Performers', 'on':'0'},
        {'id':3,'lnk':'movies', 'nm': 'Movies','on':'0'},
        {'id':2,'lnk':'barsandclubs', 'nm': 'Bars and Clubs', 'on':'0'},
        {'id':1,'lnk':'restaurants', 'nm': 'Restaurants', 'on':'0'},
        {'id':0,'lnk':'thingstodo', 'nm': 'Things to Do', 'on':'0'},
        {'lnk':'friends', 'nm': 'My Friend''s Lists', 'on':'0'},
        {'lnk':'me', 'nm': 'My Lists', 'on':'0'}]

        
import cgi
import os
import urllib2
import urllib
import unicodedata
import facebook

import base64
import cgi
import Cookie
import email.utils
import hashlib
import hmac
import logging
import time
import wsgiref.handlers

from urlparse import urlparse

from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.ext import search
from google.appengine.api import urlfetch

from django.utils import simplejson as json
from django import forms
from xml.dom import minidom

class User(db.Model):

    id = db.StringProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)
    updated = db.DateTimeProperty(auto_now=True)
    name = db.StringProperty(required=True)
    profile_url = db.StringProperty(required=True)
    access_token = db.StringProperty(required=True)

class Item_(db.Model):
    name = db.StringProperty
    url = db.StringProperty
    img = db.StringProperty
    
class List_(search.SearchableModel):

    fbUID = db.StringProperty()
    fbName = db.StringProperty()
    name = db.StringProperty()    
    iCategory = db.StringProperty()
    iSubcategory = db.StringProperty()
 
    arrNames = db.StringListProperty()
    arrUrl = db.StringListProperty()
    arrImg = db.StringListProperty()

    date = db.DateTimeProperty(auto_now_add=True)

    iTopic = db.IntegerProperty()
    likes = db.IntegerProperty()
    location = db.StringProperty()

def remove_accents(str):
                        
    nkfd_form = unicodedata.normalize('NFKD', unicode(str))
    only_ascii = nkfd_form.encode('ASCII', 'ignore')
    return only_ascii

#Build Lists to be viewed
def buildAListSet(aList):
    
    t = []
    
    for i in range(0,len(aList.arrNames)):
        t.append({
            'name': remove_accents(aList.arrNames[i]),
            'url': aList.arrUrl[i],
            'img': aList.arrImg[i]
        })

    #h = self.graph.request(thisList["fbUID"])
    #fname = h["first_name"];

    #self.response.out.write(">>>")
    #self.response.out.write(aList.key)
    return {
        'catID': aList.iCategory,
        'key': aList.key(),
        'name': aList.name,
        'fbUID': aList.fbUID,
        'items': t
    }
            
def buildListSet(tlists):
    
    tarrAll = []
    for l in tlists:
        tarrAll.append(buildAListSet(l))

    return tarrAll


class BaseHandler(webapp.RequestHandler):

    @property
    def current_user(self):

        #if not self.request.get("code"):
        #    args = dict(client_id=FACEBOOK_APP_ID, redirect_uri=self.request.path_url, scope="publish_stream")
        #    self.redirect("https://graph.facebook.com/oauth/authorize?" + urllib.urlencode(args))

        """Returns the logged in Facebook user, or None if unconnected."""
        if not hasattr(self, "_current_user"):
                self._current_user = None
        
                user_id = parse_cookie(self.request.cookies.get("fb_user"))
                if user_id:
                    self._current_user = User.get_by_key_name(user_id)
        return self._current_user
    
    @property
    def graph(self):
        """Returns a Graph API client for the current user."""
        if not hasattr(self, "_graph"):
            if self.current_user:
                self._graph = facebook.GraphAPI(self.current_user.access_token)
            else:
                self._graph = facebook.GraphAPI()
        return self._graph

    def render(self, path, **kwargs):
        args = dict(current_user=self.current_user,
                    facebook_app_id=FACEBOOK_APP_ID)
        args.update(kwargs)
        path = os.path.join(os.path.dirname(__file__), "templates", path)
        self.response.out.write(template.render(path, args))


class LoginHandler(BaseHandler):

    def get(self):
        verification_code = self.request.get("code")
        args = dict(client_id=FACEBOOK_APP_ID, redirect_uri=self.request.path_url, scope="publish_stream")

        if self.request.get("code"):
            args["client_secret"] = FACEBOOK_APP_SECRET
            args["code"] = self.request.get("code")

            response = cgi.parse_qs(urllib.urlopen(
                "https://graph.facebook.com/oauth/access_token?" +
                urllib.urlencode(args)).read())
            access_token = response["access_token"][-1]

            # Download the user profile and cache a local instance of the
            # basic profile info
            profile = json.load(urllib.urlopen(
                "https://graph.facebook.com/me?" +
                urllib.urlencode(dict(access_token=access_token))))
            user = User(key_name=str(profile["id"]), id=str(profile["id"]),
                        name=profile["name"], access_token=access_token,
                        profile_url=profile["link"])
            user.put()
            set_cookie(self.response, "fb_user", str(profile["id"]),
                       expires=time.time() + 30 * 86400)

            self.redirect(FB_APP_LINK)

        else:
            
            self.response.out.write("""
                <SCRIPT language="JavaScript"> 
                <!--
                    top.location="https://graph.facebook.com/oauth/authorize?""" + 
                    urllib.urlencode(args) + """";
                //--> 
                </SCRIPT>""")

class LogoutHandler(BaseHandler):
    def get(self):
        set_cookie(self.response, "fb_user", "", expires=time.time() - 86400)
        self.redirect("/")


def set_cookie(response, name, value, domain=None, path="/", expires=None):

    """Generates and signs a cookie for the give name/value"""
    timestamp = str(int(time.time()))
    value = base64.b64encode(value)
    signature = cookie_signature(value, timestamp)
    cookie = Cookie.BaseCookie()
    cookie[name] = "|".join([value, timestamp, signature])
    cookie[name]["path"] = path
    if domain: cookie[name]["domain"] = domain
    if expires:
        cookie[name]["expires"] = email.utils.formatdate(
            expires, localtime=False, usegmt=True)
    response.headers._headers.append(("Set-Cookie", cookie.output()[12:]))


def parse_cookie(value):

    """Parses and verifies a cookie value from set_cookie"""
    if not value: return None
    parts = value.split("|")
    if len(parts) != 3: return None
    if cookie_signature(parts[0], parts[1]) != parts[2]:
        logging.warning("Invalid cookie signature %r", value)
        return None
    timestamp = int(parts[1])
    if timestamp < time.time() - 30 * 86400:
        logging.warning("Expired cookie %r", value)
        return None
    try:
        return base64.b64decode(parts[0]).strip()
    except:
        return None


def cookie_signature(*parts):
    """Generates a cookie signature. 
    We use the Facebook app secret since it is different for every app (so
    people using this example don't accidentally all use the same secret).
    """

    hash = hmac.new(FACEBOOK_APP_SECRET, digestmod=hashlib.sha1)
    for part in parts: hash.update(part)
    return hash.hexdigest()


class locationJson(webapp.RequestHandler):

    def get(self):
        #self.response.headers['Content-Type'] = 'text/plain'
        action = "search"
        set = "events"
        subset = ""

        if int(iCategory) == "Venues":
            set = "venues"
             
        if int(iCategory) == CAT_RESTAURANTS:
            action = "search_for_restaurants"
            set = "venues"
            
        if int(iCategory) == CAT_PERFORMERS:
            action = "search_for_performers"
            subset = "performers"

        API = "http://www.zvents.com/partner_rest/" + action

        params = {
                  'format': 'json',
                  'key': 'FUKPPNUWOKOXTJSCYNBUKRJHEPUMAVGLXSTBCOGUFUJEUITWYBMOOWPONEPIEBLG',
                  'where': 'San Francisco',
                  'what': self.request.get("what")
                  #'cat': '' #iSubcategory
        }   
        
        req = urllib2.Request(API,urllib.urlencode(params))
        ret = urllib2.urlopen(req)
        self.response.out.write(ret.read())
        #urllib2.urlopen(req).

def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)


class oneList(webapp.RequestHandler):

    def get(self):

        lID = self.request.get("lid")
#        if not self.request.get("code"):
#            args = dict(client_id=FACEBOOK_APP_ID, redirect_uri=self.request.path_url, scope="publish_stream")
#            self.redirect("https://graph.facebook.com/oauth/authorize?" + urllib.urlencode(args))

        listTemplate = "list.html"
        alist = List_.all()

        #GET THIS LISTS
        alist.filter("__key__ =", db.Key(lID))
        
        thisList = buildAListSet(alist[0])

        #Get All Lists
        lists = List_.all()
        lists.filter("fbUID =", thisList["fbUID"])
        arrAll = buildListSet(lists)

        nav = "friends"

        #h = self.graph.request( thisList["fbUID"] )
        #start = self.current_user.name.find(" ")
        #fName = self.current_user.name

        template_values = {
            'fName': "",
            'lID': lID,
            'navIndex': nav,
            'arrSubNav': ZV_SUBNAV,
            'arrList': arrAll,
            'tList': thisList,
            'current_user': 1,
            'facebook_app_id': FACEBOOK_APP_ID,
            'location': "San Francisco",
            'list_link': FB_APP_LINK,
            'APP_LINK': APP_LINK
            #'app_link': APP_LINK
        }

        #self.response.out.write(arr_cat["category"])
        path = os.path.join(os.path.dirname(__file__), 'src/' + listTemplate)
        self.response.out.write(template.render(path, template_values))
    
def getCatID(name):

    for c in ZV_CATEGORIES["cats"]:
        
        if name == c["lnk"]:
            return c["cat"]
        
    return 0

class ListHandler(BaseHandler):

    def showOneList(self, lID):

        listTemplate = "list.html"
        if self.request.get("lnk") == "1":
            listTemplate = "link.html"

        alist = List_.all()

        #GET THIS LISTS
        alist.filter("__key__ =", db.Key(lID))
        
        thisList = buildAListSet(alist[0])

        #Get All Lists
        lists = List_.all()
        lists.filter("fbUID =", thisList["fbUID"])
        arrAll = buildListSet(lists)

        nav = "friends"
        #h = self.graph.request( thisList["fbUID"] )

        #start = self.current_user.name.find(" ")
        #fName = self.current_user.name
        fName = ""

        template_values = {
            'fName': fName,
            'lID': lID,
            'navIndex': nav,
            'arrSubNav': ZV_SUBNAV,
            'arrList': arrAll,
            'tList': thisList,
            'current_user': self.current_user,
            'facebook_app_id': FACEBOOK_APP_ID,
            'location': "San Francisco",
            'list_link': FB_APP_LINK,
            'APP_LINK': APP_LINK
        }

        #self.response.out.write(arr_cat["category"])
        path = os.path.join(os.path.dirname(__file__), 'src/' + listTemplate)
        self.response.out.write(template.render(path, template_values))

    def showLists(self, listType):

#        if not hasattr(self, "_current_user"):
#            self.redirect('/auth/login')

        #args = dict(client_id=FACEBOOK_APP_ID, scope="publish_stream")
        
        args = dict(client_id=FACEBOOK_APP_ID, redirect_uri=APP_LINK + "auth/login", scope="publish_stream")
        tLink = "https://graph.facebook.com/oauth/authorize?" + urllib.urlencode(args)

        bExcept = False
        nav = ""
        thisList = []
        lists = []

        if listType == LIST_CAT or listType == LIST_SEARCH:
        
                if listType == LIST_SEARCH:
            
                    sSearch = self.request.get("search")
                    lists = List_.all()
                    lists.order("-date")
                    lists.search(sSearch)
    
                if listType == LIST_CAT:
    
                    url = self.request.url
                    #self.response.out.write(url)
                    p = urlparse(url)
                    page = p[2]
                    #self.response.out.write(page)
                    page = page[1:]
                    
                    #self.response.out.write(page)
    
                    nav = page
                    lists = List_.all()
                    lists.order("-date")
                    lists.filter("iCategory =", str(getCatID(nav)))
                    
                if listType == LIST_ALL:
    
                    nav = "all"
                    lists = List_.all()
                    lists.order("-date")

        else:
        
            if self.current_user or IS_LIVE == False:

                #arrSubNav = ZV_SUBNAV
                #arrSubNav[lType]['on'] = '1';
                if listType == LIST_FRIENDS:
        
                    nav = "friends"
                    args = {}
                        
                    if IS_LIVE:
                        uid = self.current_user.id
                    else:
                        uid = "708697131"
        
                    args["query"] = "SELECT uid FROM user WHERE is_app_user=1 AND uid IN (SELECT uid2 FROM friend WHERE uid1=" + uid + ")"
        
                    try:
                        h = self.graph.fql_request(args)
                        xmldoc = minidom.parseString(h)
        
                        #Get all friends who use this application
                        arrUid = []
                        for i in xmldoc.getElementsByTagName("user"):
                            arrUid.append(getText(i.getElementsByTagName("uid")[0].childNodes))
            
                        lists = List_.gql("WHERE fbUID IN :arrUid order by date DESC LIMIT 15", arrUid=arrUid)
                        
                    except:
                        bExcept = True
                        self.response.out.write("THE REDIRECT")
                        args = dict(client_id=FACEBOOK_APP_ID, redirect_uri=self.request.path_url, scope="publish_stream")
                        self.redirect("https://graph.facebook.com/oauth/authorize?" + urllib.urlencode(args))
                        return
    
                    #lists = List_.gql("WHERE fbUID IN :arrUid", arrUid=arrUid)
                if listType == LIST_ME:
                    
                    nav = "me"
                    lists = List_.all()
                    
                    if IS_LIVE:
                        uID = self.current_user.id
                    else:
                        uID = "708697131"
    
                    lists.filter("fbUID =", str(uID))
                    lists.order("-date")
    
        if not bExcept:

                arrAll = buildListSet(lists)
                        
                template_values = {
                        'login_link': tLink,
                        'navIndex': nav,
                        'arrSubNav': ZV_SUBNAV,
                        'arrLists': arrAll,
                        'current_user': self.current_user,
                        'facebook_app_id': FACEBOOK_APP_ID,
                        'location': "San Francisco",
                        'list_link': FB_APP_LINK,
                        'APP_LINK': APP_LINK
                }
        
        else:
            
            template_values = {
                'login_link': tLink,
                'navIndex': -1,
                'arrSubNav': ZV_SUBNAV,
                'facebook_app_id': FACEBOOK_APP_ID,
                'location': "San Francisco",
                'list_link': FB_APP_LINK,
                'APP_LINK': APP_LINK
            }
            
        #self.response.out.write(arr_cat["category"])
        if not bExcept:
            path = os.path.join(os.path.dirname(__file__), 'src/index.html')
            self.response.out.write(template.render(path, template_values))

class like(ListHandler):
    
    def get(self):
        
        url = self.request.url
        p = urlparse(url)
        arrUrl = p[2]

        arrDir = arrUrl.split("/");
        
        #self.response.out.write(arrDir[2])
        ListHandler.showOneList(self, arrDir[2])
        
class mylists(ListHandler):

    def get(self):
        
        #if not self.request.get("code"):
        #    args = dict(client_id=FACEBOOK_APP_ID, redirect_uri=self.request.path_url, scope="publish_stream")
        #    self.redirect("https://graph.facebook.com/oauth/authorize?" + urllib.urlencode(args))

        ListHandler.showLists(self, LIST_ME)

class searchlists(ListHandler):
    
    def post(self):
        
        ListHandler.showLists(self, LIST_SEARCH)

class friendlists(ListHandler):
    
    def get(self):

        #if not self.request.get("code"):
        #    args = dict(client_id=FACEBOOK_APP_ID, redirect_uri=self.request.path_url, scope="publish_stream")
        #    self.redirect("https://graph.facebook.com/oauth/authorize?" + urllib.urlencode(args))

        if self.request.get("lid"):
            ListHandler.showOneList(self, self.request.get("lid"))

        else:
            ListHandler.showLists(self, LIST_FRIENDS)

class alllists(ListHandler):

    def get(self):

        #if not self.request.get("code"):
        #    args = dict(client_id=FACEBOOK_APP_ID, redirect_uri=self.request.path_url, scope="publish_stream")
        #    self.redirect("https://graph.facebook.com/oauth/authorize?" + urllib.urlencode(args))

        ListHandler.showLists(self, LIST_CAT)


class DeleteData(webapp.RequestHandler):
    def get(self):
        q = db.GqlQuery("SELECT * FROM List_")
        results = q.fetch(100)
        db.delete(results)
        self.response.out.write('Deleted')


application = webapp.WSGIApplication(
                                     [('/', friendlists),
                                      ('/like/.*', like),
                                      ('/list', oneList),
                                      ('/search', searchlists),
                                      ('/thingstodo', alllists),
                                      ('/restaurants', alllists),
                                      ('/barsandclubs', alllists),
                                      ('/movies', alllists),
                                      ('/performers', alllists),
                                      ('/places', alllists),
                                      ('/me', mylists),
                                      ('/friends', friendlists),
                                      ('/delete', DeleteData),
                                      ("/auth/login", LoginHandler),
                                      ("/auth/logout", LogoutHandler)],
                                     debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()