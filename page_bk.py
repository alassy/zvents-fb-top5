FACEBOOK_APP_ID = "115973285117078"
FACEBOOK_APP_SECRET = "3db24093a7572749bcba7caa314b3b12"

LIKE_POST_LINK = "http://zvlists.appspot.com/"
FB_APP_LINK = "http://apps.facebook.com/top__five/"

#IS_LIVE = False
IS_LIVE = True

APP_LINK = "http://localhost:8080/"
if IS_LIVE:
    APP_LINK = "http://zvlists.appspot.com/"


CAT_VENUES = 3
CAT_RESTAURANTS = 4
CAT_PERFORMERS = 5

LIST_ALL = 0
LIST_FRIENDS = 6
LIST_ME = 7

ZV_CATEGORY = """[
        {
            "id":0,"name":"Arts","api":"search","set":"event"
        },{
            "id":1,"name":"Live Music & Performance","api":"search","set":"event"
        },{
            "id":2,"name":"Sports & Outdoors","api":"search","set":"event"
        },{
            "id":3,"name":"Venues","api":"search","set":"venue"
        },{
            "id":4,"name":"Restaurants","api":"search_for_restaurants","set":"venue"
        },{
            "id":5,"name":"Performers","api":"search_for_performers","set":"group"
        },{
            "id":6,"name":"Movies","api":"search","set":"event"
        }]"""

ZV_SUBNAV = [
            {'lnk':'places', 'nm': 'Places', 'on':'0' },
            {'lnk':'performers', 'nm': 'Performers', 'on':'0'},
            {'lnk':'movies', 'nm': 'Movies','on':'0'},
            {'lnk':'barsandclubs', 'nm': 'Bars and Clubs', 'on':'0'},
            {'lnk':'restaurants', 'nm': 'Restaurants', 'on':'0'},
            {'lnk':'thingstodo', 'nm': 'Things to Do', 'on':'0'},
            {'lnk':'friends', 'nm': 'My Friend''s Lists', 'on':'0'},
            {'lnk':'me', 'nm': 'My Lists', 'on':'0'}        
        ]
        
import cgi
import os
import urllib2
import urllib
import unicodedata

import facebook

from google.appengine.api import users

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.ext import search
from google.appengine.api import urlfetch

from django.utils import simplejson as json
from django import forms
from xml.dom import minidom

class User(db.Model):

    id = db.StringProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)
    updated = db.DateTimeProperty(auto_now=True)
    name = db.StringProperty(required=True)
    profile_url = db.StringProperty(required=True)
    access_token = db.StringProperty(required=True)

class Item_(db.Model):
    name = db.StringProperty
    url = db.StringProperty
    img = db.StringProperty
    
class List_(search.SearchableModel):

    fbUID = db.StringProperty()
    name = db.StringProperty()    
    iCategory = db.StringProperty()
    iSubcategory = db.StringProperty()
 
    arrNames = db.StringListProperty()
    arrUrl = db.StringListProperty()
    arrImg = db.StringListProperty()

    date = db.DateTimeProperty(auto_now_add=True)

    likes = db.IntegerProperty()
    location = db.StringProperty()

def remove_accents(str):
                        
    nkfd_form = unicodedata.normalize('NFKD', unicode(str))
    only_ascii = nkfd_form.encode('ASCII', 'ignore')
    return only_ascii

#Build Lists to be viewed
def buildAListSet(aList):
    
    t = []
    
    for i in range(0,len(aList.arrNames)):
        t.append({
            'name': remove_accents(aList.arrNames[i]),
            'url': aList.arrUrl[i],
            'img': aList.arrImg[i]
        })
    
    #h = self.graph.request(thisList["fbUID"])
    #fname = h["first_name"];

    #self.response.out.write(">>>")
    #self.response.out.write(aList.key)
    return {
        'key': aList.key(),
        'name': aList.name,
        'fbUID': aList.fbUID,
        'items': t
    }
            
def buildListSet(tlists):
    
    tarrAll = []
    for l in tlists:
        tarrAll.append(buildAListSet(l))

    return tarrAll


class BaseHandler(webapp.RequestHandler):

    """Provides access to the active Facebook user in self.current_user
    The property is lazy-loaded on first access, using the cookie saved
    by the Facebook JavaScript SDK to determine the user ID of the active
    user. See http://developers.facebook.com/docs/authentication/ for
    more information.
    """
    @property
    def current_user(self):

            if not hasattr(self, "_current_user"):
                self._current_user = None
                cookie = facebook.get_user_from_cookie(
                    self.request.cookies, FACEBOOK_APP_ID, FACEBOOK_APP_SECRET)
                if cookie:
                    # Store a local instance of the user data so we don't need
                    # a round-trip to Facebook on every request
                    user = User.get_by_key_name(cookie["uid"])
                    if not user:
                        graph = facebook.GraphAPI(cookie["access_token"])
                        profile = graph.get_object("me")
                        user = User(key_name=str(profile["id"]),
                                    id=str(profile["id"]),
                                    name=profile["name"],
                                    profile_url=profile["link"],
                                    access_token=cookie["access_token"])
                        user.put()
                    elif user.access_token != cookie["access_token"]:
                        user.access_token = cookie["access_token"]
                        user.put()
                    self._current_user = user
            return self._current_user

    @property
    def graph(self):
        """Returns a Graph API client for the current user."""
        if not hasattr(self, "_graph"):
            if self.current_user:
                self._graph = facebook.GraphAPI(self.current_user.access_token)
            else:
                self._graph = facebook.GraphAPI()
        return self._graph

    def render(self, path, **kwargs):
        args = dict(current_user=self.current_user,
                    facebook_app_id=FACEBOOK_APP_ID)
        args.update(kwargs)
        path = os.path.join(os.path.dirname(__file__), "templates", path)
        self.response.out.write(template.render(path, args))

class locationJson(webapp.RequestHandler):

    def get(self):

        #self.response.headers['Content-Type'] = 'text/plain'
        action = "search"
        set = "events"
        subset = ""

        if int(iCategory) == "Venues":
            set = "venues"
             
        if int(iCategory) == CAT_RESTAURANTS:
            action = "search_for_restaurants"
            set = "venues"
            
        if int(iCategory) == CAT_PERFORMERS:
            action = "search_for_performers"
            subset = "performers"

        API = "http://www.zvents.com/partner_rest/" + action

        params = {
                  'format': 'json',
                  'key': 'FUKPPNUWOKOXTJSCYNBUKRJHEPUMAVGLXSTBCOGUFUJEUITWYBMOOWPONEPIEBLG',
                  'where': 'San Francisco',
                  'what': self.request.get("what")
                  #'cat': '' #iSubcategory
        }   
        
        req = urllib2.Request(API,urllib.urlencode(params))
        ret = urllib2.urlopen(req)
        self.response.out.write(ret.read())
        #urllib2.urlopen(req).

def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)


class ListHandler(BaseHandler):

    def showOneList(self, lID):
        
        #lists = List_.all()
        alist = List_.all()

        #GET THIS LISTS
        alist.filter("__key__ =", db.Key(lID))
        
        thisList = buildAListSet(alist[0])

        #Get All Lists
        lists = List_.all()
        lists.filter("fbUID =", thisList["fbUID"])
        arrAll = buildListSet(lists)

        nav = "friends"
        h = self.graph.request(thisList["fbUID"])

        #start = self.current_user.name.find(" ")
        fName = self.current_user.name
        

        template_values = {
            'fName': fName,
            'lID': lID,
            'navIndex': nav,
            'arrSubNav': ZV_SUBNAV,
            'arrList': arrAll,
            'tList': thisList,
            'current_user': self.current_user,
            'facebook_app_id': FACEBOOK_APP_ID,
            'location': "San Francisco",
            'list_link': LIKE_POST_LINK,
            'app_link': FB_APP_LINK
        }

        #self.response.out.write(arr_cat["category"])
        path = os.path.join(os.path.dirname(__file__), 'src/list.html')
        self.response.out.write(template.render(path, template_values))

    def showLists(self, listType):

        if self.current_user or IS_LIVE == False:

            nav = ""
            thisList = []
            #arrSubNav = ZV_SUBNAV
            #arrSubNav[lType]['on'] = '1';
    
            lists = []
            if listType == LIST_ALL:
                nav = "all"
                lists = List_.all()
                lists.order("-date")
                
            if listType == LIST_FRIENDS:
    
                nav = "friends"
                args = {}
                    
                if IS_LIVE:
                    uid = self.current_user.id
                else:
                    uid = "708697131"
    
                args["query"] = "SELECT uid FROM user WHERE is_app_user=1 AND uid IN (SELECT uid2 FROM friend WHERE uid1=" + uid + ")"
    
                h = self.graph.fql_request(args)
                xmldoc = minidom.parseString(h)
    
                #Get all friends who use this application
                arrUid = []
                for i in xmldoc.getElementsByTagName("user"):
                    arrUid.append(getText(i.getElementsByTagName("uid")[0].childNodes))
    
                lists = List_.gql("WHERE fbUID IN :arrUid order by date DESC", arrUid=arrUid)
                #lists = List_.gql("WHERE fbUID IN :arrUid", arrUid=arrUid)

            if listType == LIST_ME:
                
                nav = "me"
                lists = List_.all()
                
                if IS_LIVE:
                    uID = self.current_user.id
                else:
                    uID = "708697131"

                lists.filter("fbUID =", str(uID))
    
                lists.order("-date")
    
            arrAll = buildListSet(lists)
                
            template_values = {
                'navIndex': nav,
                'arrSubNav': ZV_SUBNAV,
                'arrLists': arrAll,
                'current_user': self.current_user,
                'facebook_app_id': FACEBOOK_APP_ID,
                'location': "San Francisco",
                'list_link': LIKE_POST_LINK,
                'app_link': FB_APP_LINK
            }
        
        else:
            
            template_values = {
                'navIndex': -1,
                'arrSubNav': ZV_SUBNAV,
                'facebook_app_id': FACEBOOK_APP_ID,
                'location': "San Francisco",
                'list_link': LIKE_POST_LINK,
                'app_link': FB_APP_LINK
            }
            
        #self.response.out.write(arr_cat["category"])
        path = os.path.join(os.path.dirname(__file__), 'src/index.html')
        self.response.out.write(template.render(path, template_values))

class mylists(ListHandler):

    def get(self):
        ListHandler.showLists(self, LIST_ME)


class friendlists(ListHandler):
    
    def get(self):
        
        if self.request.get("lid"):
            ListHandler.showOneList(self, self.request.get("lid"))

        else:
            ListHandler.showLists(self, LIST_FRIENDS)

class alllists(ListHandler):

    def get(self):
        ListHandler.showLists(self, LIST_ALL)


class DeleteData(webapp.RequestHandler):
    def get(self):
        q = db.GqlQuery("SELECT * FROM List_")
        results = q.fetch(100)
        db.delete(results)
        self.response.out.write('Deleted')



application = webapp.WSGIApplication(
                                     [('/', friendlists),
                                      ('/thingstodo', alllists),
                                      ('/restaurants', alllists),
                                      ('/barsandclubs', alllists),
                                      ('/movies', alllists),
                                      ('/performers', alllists),
                                      ('/places', alllists),
                                      ('/me', mylists),
                                      ('/friends', friendlists),
                                      ('/delete', DeleteData)],
                                     debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()